"""
Author: Andrey A. Babichev

andrey.a.babichev@gmail.com
"""
import time

import kernelchip
import socket
import socketserver
import threading
import unittest

HOST = "127.0.0.1"
PORT = 2424


class TestServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    allow_reuse_address = True


class TestTCPHandler(socketserver.BaseRequestHandler):

    def handle(self):
        # self.request is the TCP socket connected to the client
        while 1:
            cmd = self.request.recv(1024).strip().decode()
            if not cmd: break

            result = "#Error"

            if cmd == "$KE":
                result = "#OK"
            elif cmd == "$KE,PSW,SET,%s" % self.server.password:
                result = "#PSW,SET,OK"

            if result is not None:
                self.request.sendall(b"%s\r\n" % result.encode())


class KBX3DTest(unittest.TestCase):

    def setUp(self):
        self.server = TestServer((HOST, PORT), TestTCPHandler)
        self.server.password = "KBX-3D"
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        self.client = socket.create_connection((HOST, PORT))
        #self.server_thread.setDaemon(True)
        self.server_thread.start()

    def tearDown(self):
        self.client.close()
        self.server.shutdown()
        self.server.server_close()

    def test_is_live(self):
        protocol = kernelchip.KernelChipTCPProtocol(HOST, port=PORT)
        kbx3d = kernelchip.KernelChip(protocol, password="KBX-3D")
        self.assertTrue(kbx3d.is_live())

    def _test_real(self):
        protocol = kernelchip.KernelChipTCPProtocol("192.168.1.55", port=PORT)
        kbx3d = kernelchip.KernelChip(protocol, password="KBX-3D")
        self.assertTrue(kbx3d.is_live())
        print(kbx3d.get_relay(1))
        print(kbx3d.relay_on(1))
        print(kbx3d.get_relay(1))
        time.sleep(5)
        print(kbx3d.relay_off(1))
        print(kbx3d.get_relay(1))


__all__ = (
    'KBX3DTest',
)