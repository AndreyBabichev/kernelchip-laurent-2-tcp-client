"""
Author: Andrey A. Babichev

andrey.a.babichev@gmail.com
"""

import logging
import socket


logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()

#fileHandler = logging.FileHandler("{0}/{1}.log".format(logPath, fileName))
#fileHandler.setFormatter(logFormatter)
#rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)


class AbstractKernelChipProtocol(object):

    def send_command(self, cmd):
        raise NotImplemented


class KernelChipTCPProtocol(AbstractKernelChipProtocol):

    logger = rootLogger

    def __init__(self, ip, port=2424):
        self.ip = ip
        self.port = port
        self._socket = None
        self._connected = False

    def connect(self):
        self._socket = socket.create_connection((self.ip, self.port))
        self._connected = True

    def disconnect(self):
        self._connected = False
        self._socket.close()

    def send_command(self, cmd):
        if not self._connected:
            self.connect()

        self.logger.debug("Sending command: %s" % cmd)

        self._socket.sendall(b"%s\r\n" % cmd.encode())

        result = self._socket.recv(1024).strip()

        self.logger.debug("Accepted data: %s" % result)

        return result.decode()

    def __exit__(self, exc_type, exc_val, exc_tb):
        # TODO try/catch
        self.disconnect()


class KernelChip(object):

    class KernelChipError(Exception):
        pass

    class CannotLogon(KernelChipError):
        def __init__(self):
            pass

    def __init__(self, protocol, password=None):
        """
        :type protocol: AbstractKernelChipProtocol
        """
        self.protocol = protocol
        self.password = password
        self._login = False

    def send_command(self, cmd):
        if not self._login:
            self.login()

        return self.protocol.send_command(cmd)

    def login(self):
        if self.protocol.send_command("$KE,PSW,SET,%s" % self.password) == "#PSW,SET,OK":
            self._login = True
            return

        self._login = False

        raise self.CannotLogon()

    def is_live(self):
        if self.send_command("$KE") == "#OK":
            return True
        return False

    def get_relay(self, rel_num):
        if self.send_command("$KE,RDR,%s" % rel_num) == "#RDR,%s,1" % rel_num:
            return True
        return False

    def set_relay(self, rel_num, val):
        if self.send_command("$KE,REL,%s,%s" % (rel_num, val)) == "#REL,OK":
            return True
        return False

    def relay_on(self, rel_num):
        return self.set_relay(rel_num, 1)

    def relay_off(self, rel_num):
        return self.set_relay(rel_num, 0)
